/**
 * 2017年12月29日
 */
package com.gitee.weixin.mp.handler.msg.text;

import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.WxMpKefuMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户获取openid
 *
 * @author aaron 2017年12月29日
 */
@Component
public class MyOpenidTransaction extends AbstractTxTransaction {

    @PostConstruct
    public void init() {
        List<String> parttens = new ArrayList<>();
        parttens.add("MyOpenid");
        // parttens.add(""); 可以是正则表达式

        this.setRegexStr(parttens);

        logger.info("###### {} init success.", this);
    }

    @Override
    public WxMpXmlOutMessage doTextTrans(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService,
                                         WxSessionManager sessionManager) {
        WxMpKefuMessage kefuMessage = WxMpKefuMessage.TEXT().toUser(wxMessage.getFromUser()).content(wxMessage.getFromUser()).build();
        try {
            wxMpService.getKefuService().sendKefuMessage(kefuMessage);
        } catch (WxErrorException e) {
            logger.error(e.getError().getJson(), e);
        }
        return null;
    }

}
