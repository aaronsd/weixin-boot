/**
 * 2017年12月29日
 */
package com.gitee.weixin.mp.handler.msg.text.iface;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import java.util.Map;

/**
 * 文本处理交易
 *
 * @author aaron 2017年12月29日
 */
@FunctionalInterface
public interface TxTransaction {

    /**
     * 处理文本交易,采用客服消息异步回复
     *
     * @param wxMessage      微信消息，可以获取文本内容和发送者等信息
     * @param context        微信消息上下文
     * @param wxMpService    与公众号通讯等的处理服务
     * @param sessionManager 会话管理
     */
    WxMpXmlOutMessage doTextTrans(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService, WxSessionManager sessionManager);
}
