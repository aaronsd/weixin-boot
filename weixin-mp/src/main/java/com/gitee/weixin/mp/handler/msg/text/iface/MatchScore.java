/**
 * 2017年12月29日
 */
package com.gitee.weixin.mp.handler.msg.text.iface;

/**
 * 计算文本匹配得分
 *
 * @author aaron 2017年12月29日
 */
@FunctionalInterface
public interface MatchScore {
    /**
     * 计算文本的匹配得分
     *
     * @param text
     * @return
     */
    double matchScore(String text);
}
