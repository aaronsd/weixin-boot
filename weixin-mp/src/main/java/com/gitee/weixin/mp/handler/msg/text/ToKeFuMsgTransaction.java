/**
 * 2017年12月29日
 */
package com.gitee.weixin.mp.handler.msg.text;

import com.gitee.weixin.mp.builder.TextBuilder;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.common.session.WxSession;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.kefu.result.WxMpKfInfo;
import me.chanjar.weixin.mp.bean.kefu.result.WxMpKfOnlineList;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 转发给客服的消息处理交易
 *
 * @author aaron 2017年12月29日
 */
@Component
public class ToKeFuMsgTransaction extends AbstractTxTransaction {
    public static final String SESSION_KEY_LASTKEFU = "GuessNumberTransaction.SESSION_KEY_LASTKEFU";
    public static final int    ERR_CODE_65400       = 65400;

    @PostConstruct
    public void init() {
        List<String> parttens = new ArrayList<>();
        parttens.add("客服");
        parttens.add("您好");
        parttens.add("你好");
        parttens.add("hello");
        parttens.add("hi");
        parttens.add("请问");
        parttens.add("怎么办");
        parttens.add("kf");
        // parttens.add("caiping"); 可以是正则表达式

        this.setRegexStr(parttens);

        // 设置为同步交易
        this.setAsyn(false);

        logger.info("###### {} init success.", this);
    }

    @Override
    public WxMpXmlOutMessage doTextTrans(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService,
                                         WxSessionManager sessionManager) {
        String content = "";

        // 设置session
        WxSession session = sessionManager.getSession(wxMessage.getFromUser());
        session.setAttribute(SESSION_KEY_TXTRANSACTION, this);

        // 当用户输入关键词如“你好”，“客服”等，并且有客服在线时，把消息转发给在线客服
        try {
            // 有没有上次会话的客户？
            WxMpKfInfo kefu = (WxMpKfInfo) session.getAttribute(SESSION_KEY_LASTKEFU);
            if (kefu == null) {
                WxMpKfOnlineList kfList = wxMpService.getKefuService().kfOnlineList();
                List<WxMpKfInfo> listOfKf = kfList.getKfOnlineList();
                Optional<WxMpKfInfo> oneKf = listOfKf.stream().findAny();
                if (oneKf.isPresent()) {
                    session.setAttribute(SESSION_KEY_LASTKEFU, oneKf.get());
                    kefu = oneKf.get();
                }
            }
            if (kefu != null) {
                return WxMpXmlOutMessage.TRANSFER_CUSTOMER_SERVICE().fromUser(wxMessage.getToUser()).toUser(wxMessage.getFromUser())
                        .kfAccount(kefu.getAccount()).build();
            }
            // 没有客服在线
            else {
                content = "非常抱歉，客服均不在线";
                session.invalidate();
            }

        } catch (WxErrorException e) {
            logger.error(e.getError().toString());
            // TODO 可以根据不同的错误码进行处理
            // {"errcode":65400,"errmsg":"please enable new custom service, or
            // wait for a while if you have enabled hint: [rMOp70085shb1]"}
            if (ERR_CODE_65400 == e.getError().getErrorCode()) {
                content = "非常抱歉，暂时未开通客服功能";
            } else {
                content = "非常抱歉，客服均不在线";
            }
            session.invalidate();
        }

        return new TextBuilder().build(content, wxMessage, wxMpService);
    }

}
