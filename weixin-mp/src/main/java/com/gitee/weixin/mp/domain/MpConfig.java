package com.gitee.weixin.mp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MpConfig {
    @Id
    private String appId;
    private String secret;
    private String token;
    private String aesKey;

    private String accessToken;
    private long accessTokenExpiresTime = 0;
    @Column(length = 512)
    private String oauth2redirectUri;
    private String httpProxyHost;
    private int httpProxyPort = 0;
    private String httpProxyUsername;
    private String httpProxyPassword;
    private String jsapiTicket;
    private long jsapiTicketExpiresTime = 0;
    private String cardApiTicket;
    private long cardApiTicketExpiresTime = 0;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAesKey() {
        return aesKey;
    }

    public void setAesKey(String aesKey) {
        this.aesKey = aesKey;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getAccessTokenExpiresTime() {
        return accessTokenExpiresTime;
    }

    public void setAccessTokenExpiresTime(long accessTokenExpiresTime) {
        this.accessTokenExpiresTime = accessTokenExpiresTime;
    }

    public String getOauth2redirectUri() {
        return oauth2redirectUri;
    }

    public void setOauth2redirectUri(String oauth2redirectUri) {
        this.oauth2redirectUri = oauth2redirectUri;
    }

    public String getHttpProxyHost() {
        return httpProxyHost;
    }

    public void setHttpProxyHost(String httpProxyHost) {
        this.httpProxyHost = httpProxyHost;
    }

    public int getHttpProxyPort() {
        return httpProxyPort;
    }

    public void setHttpProxyPort(int httpProxyPort) {
        this.httpProxyPort = httpProxyPort;
    }

    public String getHttpProxyUsername() {
        return httpProxyUsername;
    }

    public void setHttpProxyUsername(String httpProxyUsername) {
        this.httpProxyUsername = httpProxyUsername;
    }

    public String getHttpProxyPassword() {
        return httpProxyPassword;
    }

    public void setHttpProxyPassword(String httpProxyPassword) {
        this.httpProxyPassword = httpProxyPassword;
    }

    public String getJsapiTicket() {
        return jsapiTicket;
    }

    public void setJsapiTicket(String jsapiTicket) {
        this.jsapiTicket = jsapiTicket;
    }

    public long getJsapiTicketExpiresTime() {
        return jsapiTicketExpiresTime;
    }

    public void setJsapiTicketExpiresTime(long jsapiTicketExpiresTime) {
        this.jsapiTicketExpiresTime = jsapiTicketExpiresTime;
    }

    public String getCardApiTicket() {
        return cardApiTicket;
    }

    public void setCardApiTicket(String cardApiTicket) {
        this.cardApiTicket = cardApiTicket;
    }

    public long getCardApiTicketExpiresTime() {
        return cardApiTicketExpiresTime;
    }

    public void setCardApiTicketExpiresTime(long cardApiTicketExpiresTime) {
        this.cardApiTicketExpiresTime = cardApiTicketExpiresTime;
    }

    @Override
    public String toString() {
        return "MpConfig [appId=" + appId + ", secret=" + secret + ", token=" + token + ", aesKey=" + aesKey + ", accessToken=" + accessToken
                + ", accessTokenExpiresTime=" + accessTokenExpiresTime + ", oauth2redirectUri=" + oauth2redirectUri + ", httpProxyHost="
                + httpProxyHost + ", httpProxyPort=" + httpProxyPort + ", httpProxyUsername=" + httpProxyUsername + ", httpProxyPassword="
                + httpProxyPassword + ", jsapiTicket=" + jsapiTicket + ", jsapiTicketExpiresTime=" + jsapiTicketExpiresTime + ", cardApiTicket="
                + cardApiTicket + ", cardApiTicketExpiresTime=" + cardApiTicketExpiresTime + "]";
    }
}
