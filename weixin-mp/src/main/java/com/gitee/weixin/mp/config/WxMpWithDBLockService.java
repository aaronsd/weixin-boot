package com.gitee.weixin.mp.config;

import com.gitee.weixin.mp.service.MpConfigService;
import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.impl.WxMpServiceApacheHttpClientImpl;

public class WxMpWithDBLockService extends WxMpServiceApacheHttpClientImpl {

    private MpConfigService accessTokenUpdateService;

    public WxMpWithDBLockService(MpConfigService accessTokenUpdateService) {
        this.accessTokenUpdateService = accessTokenUpdateService;
    }

    /*
     * (non-Javadoc)
     *
     * @see me.chanjar.weixin.mp.api.impl.WxMpServiceApacheHttpClientImpl#
     * getAccessToken(boolean)
     */
    @Override
    public String getAccessToken(boolean forceRefresh) throws WxErrorException {
        return accessTokenUpdateService.getAccessToken(forceRefresh, this);
    }

}
