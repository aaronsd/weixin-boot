package com.gitee.weixin.mp;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Properties;

@EnableFeignClients
@SpringCloudApplication
@EnableTransactionManagement
public class MPSpringApplication {

    public static void main(String[] args) {
        Properties prop = System.getProperties();
        prop.setProperty("log4j2.contextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");

        SpringApplication.run(MPSpringApplication.class, args);
    }
}
