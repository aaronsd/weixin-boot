package com.gitee.weixin.mp.handler.msg;

import com.gitee.weixin.mp.handler.AbstractHandler;
import me.chanjar.weixin.common.api.WxConsts.XmlMsgType;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 普通消息处理器的父类
 *
 * @author Binary Wang(https://github.com/binarywang)
 */
public abstract class AbstractWxMsgHandler extends AbstractHandler {

    static Logger logger = LoggerFactory.getLogger(AbstractWxMsgHandler.class);

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) {

        if (!wxMessage.getMsgType().equals(XmlMsgType.EVENT)) {
            // TODO 可以选择将消息保存到本地

        }

        // 父类不对消息进行其他处理
        return null;
    }

}
