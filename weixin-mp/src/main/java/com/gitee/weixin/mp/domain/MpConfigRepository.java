/**
 * 2017年12月30日
 */
package com.gitee.weixin.mp.domain;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * 微信配置dao
 *
 * @author aaron 2017年12月30日
 */
public interface MpConfigRepository extends CrudRepository<MpConfig, String> {

    Optional<MpConfig> findByAppId(String appId);

    @Modifying
    @Query("update #{#entityName} t set t.accessTokenExpiresTime = ?2 where t.appId = ?1")
    int updateAccessTokenExpiresTimeByAppId(String appId, long accessTokenExpiresTime);

    @Modifying
    @Query("update #{#entityName} t set t.accessToken = ?2 , t.accessTokenExpiresTime = ?3 where t.appId = ?1")
    int updateAccessTokenByAppId(String appId, String accessToken, long accessTokenExpiresTime);
}
