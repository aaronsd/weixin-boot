/**
 * 2017年12月28日
 */
package com.gitee.weixin.mp.handler.msg;

import com.gitee.weixin.mp.builder.TextBuilder;
import com.gitee.weixin.mp.utils.JsonUtils;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 接收到地理位置消息的处理器
 *
 * @author aaron 2017年12月28日
 */
@Component
public class LocationMsgHandler extends AbstractWxMsgHandler {

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService weixinService,
                                    WxSessionManager sessionManager) {
        // 父类进行公共处理，存储消息等
        super.handle(wxMessage, context, weixinService, sessionManager);


        // TODO 组装回复消息
        String content = "收到LocationMsg：" + JsonUtils.toJson(wxMessage);

        return new TextBuilder().build(content, wxMessage, weixinService);
    }

}
