CREATE TABLE `mp_config` (
  `app_id`                       VARCHAR(255) NOT NULL,
  `access_token`                 VARCHAR(255) DEFAULT NULL,
  `access_token_expires_time`    BIGINT(20)   DEFAULT '0',
  `aes_key`                      VARCHAR(255) DEFAULT NULL,
  `card_api_ticket`              VARCHAR(255) DEFAULT NULL,
  `card_api_ticket_expires_time` BIGINT(20)   DEFAULT '0',
  `http_proxy_host`              VARCHAR(255) DEFAULT NULL,
  `http_proxy_password`          VARCHAR(255) DEFAULT NULL,
  `http_proxy_port`              INT(11)      DEFAULT '0',
  `http_proxy_username`          VARCHAR(255) DEFAULT NULL,
  `jsapi_ticket`                 VARCHAR(255) DEFAULT NULL,
  `jsapi_ticket_expires_time`    BIGINT(20)   DEFAULT '0',
  `oauth2redirect_uri`           VARCHAR(512) DEFAULT NULL,
  `secret`                       VARCHAR(255) DEFAULT NULL,
  `token`                        VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`app_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
SELECT *
FROM iwechat.mp_config;