package com.gitee.weixin.mp.controller;

import me.chanjar.weixin.common.util.crypto.SHA1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WechatControllerTest {

    @Test
    public void test() {
        String timestamp = String.valueOf(System.currentTimeMillis());
        String token = "token";
        String nonce = "echo";

        String sign = SHA1.gen(token, timestamp, nonce);
        System.out.println(token);
        System.out.println(timestamp);
        System.out.println(nonce);
        System.out.println(sign);
    }

}
