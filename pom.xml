<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.gitee.weixin</groupId>
	<artifactId>weixin-boot</artifactId>
	<version>0.0.1</version>
	<packaging>pom</packaging>
	<name>weixin-boot</name>
	<description>weixin by spring boot</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.8.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Dalston.SR4</spring-cloud.version>
		<weixin-tools-mp.version>2.9.0</weixin-tools-mp.version>
		<weixin-tools-cp.version>2.9.0</weixin-tools-cp.version>
		<weixin-tools-pay.version>2.9.0</weixin-tools-pay.version>
		<weixin-tools-open.version>2.9.0</weixin-tools-open.version>
		<weixin-tools-app.version>2.9.0</weixin-tools-app.version>
	</properties>

	<modules>
		<!-- 服务发现，集群环境下，系统间调用使用，后期实现zuul gateway时也需要 -->
		<module>weixin-comm</module><!-- 提供自定义的公共类库 -->
		<module>weixin-mp</module><!-- 微信公众号 -->
		<module>weixin-pay</module><!-- 微信支付 -->
	</modules>

	<dependencies>
		<dependency> <!-- 引入log4j2依赖 -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
		</dependency>
		<dependency><!-- https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-web -->
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-web</artifactId>
		</dependency>
		<dependency><!-- https://mvnrepository.com/artifact/com.lmax/disruptor -->
			<groupId>com.lmax</groupId>
			<artifactId>disruptor</artifactId>
			<version>3.3.6</version>
		</dependency>

		<!-- spring starters -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-feign</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-hystrix</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
			<optional>true</optional>
		</dependency>

		<!-- 微信开发工具包 -->
		<dependency>
			<groupId>com.github.binarywang</groupId>
			<artifactId>weixin-java-mp</artifactId><!-- 公众号 -->
			<version>${weixin-tools-mp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.github.binarywang</groupId>
			<artifactId>weixin-java-pay</artifactId><!-- 微信支付 -->
			<version>${weixin-tools-pay.version}</version>
		</dependency>
		<dependency>
			<groupId>com.github.binarywang</groupId>
			<artifactId>weixin-java-cp</artifactId><!-- 企业号/企业微信 -->
			<version>${weixin-tools-cp.version}</version>
		</dependency>
		<dependency>
			<groupId>com.github.binarywang</groupId>
			<artifactId>weixin-java-miniapp</artifactId><!-- 微信小程序 -->
			<version>${weixin-tools-app.version}</version>
		</dependency>
		<dependency>
			<groupId>com.github.binarywang</groupId>
			<artifactId>weixin-java-open</artifactId><!-- 微信开放平台 -->
			<version>${weixin-tools-open.version}</version>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
		<defaultGoal>compile</defaultGoal>
	</build>


</project>
