package com.gitee.weixin.feignConfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Logger;

@Configuration
public class FeignLogConfiguration {

	@Bean
	Logger.Level feignLogger() {
		return Logger.Level.BASIC;
	}
}
