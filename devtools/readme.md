# 开发工具使用说明

## 单机版注册中心 eureka
1. weixin-eureka-1.0.jar 依赖jre8
2. 默认端口为 8761，可以修改jar包中的 BOOT-INF/classes/application.yml 进行更改
3. 日志默认输出到/home/eureka 或者 /c/home/eureka 可以通过BOOT-INF/classes/log4j2-spring.xml修改
4. 执行脚本start-eureka.sh启动注册中心
5. 启动后访问 http://localhost:8761 可以查看注册的服务

## 内网穿透工具 ngrok
1. 执行脚本start-ngrok.sh 启动ngrok
2. 访问http://localhost:4040 查看穿透后的域名
3. 在微信公众平台的后台管理中配置穿透后的url（配置前需要启动对应的应用）

## 微信开发测试号注册
https://mp.weixin.qq.com/debug/cgi-bin/sandbox?t=sandbox/login
