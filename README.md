# 基于springboot以及weixin-java-tools开发微信公众号及微信支付等应用

## 参考
- [weixin-java-tools](https://github.com/Wechat-Group/weixin-java-tools)
- [weixin-java-mp-demo-springboot](https://gitee.com/binary/weixin-java-mp-demo-springboot)

这是见过的最好的微信javaSDK，希望自己的东西也可以为别人所用 @20171227

## 工具推荐
- [微信开发必备，内网穿透工具 ngrok](https://dashboard.ngrok.com/get-started)  下载后建议放到环境变量$PATH指向的任意目录下，方便使用
- [http api 测试工具 postman](https://www.getpostman.com/)
